
# Prueba tecnica Leadbox

Hola soy Andres Marquez postulante para el puesto de desarrollador frontend, espero que les guste y me gustaria mucho su feedback.

Para el desarrollo de esta prueba vue.js es mi elección principal como framework javascript debido a su gran versatilidad y la comodidad que me brinda al trabajar con él. En cuanto al diseño, opto por utilizar Tailwind CSS por su increíble flexibilidad, y para complementar mis estilos, aprovecho las ventajas de Flowbite, un framework basado en Tailwind que ofrece una amplia gama de clases y componentes personalizables. Además, no puedo olvidar mencionar la utilidad de Line Awesome como biblioteca de iconos. Con todo esto en mente, ahora podemos proceder con la instalacion.
## Instalacion

Clona el proyecto

```bash
  git clone https://gitlab.com/andres_marquez02/leadbox.git
```

Abre el directorio donde esta almacenado el proyecto

```bash
  cd leadbox
```

Instala las dependecias

```bash
  npm install
```

y listo!! echa a correre el proyecto con el siguiente comando

```bash
  npm run dev 
```
si quieres visualizarlo en tu dispositivo movil ejecuta el siguitente comando

```bash
  npm run dev -- --host
```
Al ejecutar este comando nos dara la direccion ip en la cual esta esta corriendo el proyecto, solo escribe esta direccion el tu navegador y distruta de el.


Muchas gracias por su tiempo y dedicacion