import { createApp } from 'vue'
import Index from './views/home/Index.vue'

import './index.css'

createApp(Index).mount('#app')